#ifndef __SCORESCENE_SCENE_H__
#define __SCORESCENE_SCENE_H__

#include "cocos2d.h"

class ScoreScene : public cocos2d::LayerGradient
{
public:
	static cocos2d::Scene* createScene();

	virtual bool init();

	// a selector callback
	void menuCloseCallback(cocos2d::Ref* pSender);

	CREATE_FUNC(ScoreScene);

	cocos2d::Menu* createMenu();
	void PlayGame(cocos2d::Ref* pSender);
	void BackToMenu(cocos2d::Ref* pSender);
};

#endif // __SCORESCENE_SCENE_H__
